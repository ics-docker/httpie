httpie
======

Docker_ image to run `httpie <https://httpie.org>`_, a command line HTTP client.

.. _Docker: https://www.docker.com
