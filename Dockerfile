FROM debian:stretch-slim

LABEL maintainer "benjamin.bertrand@esss.se"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
  && apt-get install -yq --no-install-recommends \
    httpie=0.9.8-1 \
    jq \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN useradd -m -s /bin/bash -g users csi

ENV LANG C.UTF-8
USER csi
